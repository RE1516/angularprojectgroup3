import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-productmain',
  templateUrl: './productmain.component.html',
  styleUrls: ['./productmain.component.scss']
})
export class ProductmainComponent implements OnInit {
  @Input() product: [];
  constructor() { }

  ngOnInit(): void {
  }

}
