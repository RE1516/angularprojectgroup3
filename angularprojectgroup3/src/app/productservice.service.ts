import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductserviceService {

  constructor(private httpClient: HttpClient) { }

  // tslint:disable-next-line: typedef
  getProducts() {
    return this.httpClient.get('https://my-json-server.typicode.com/franlindebl/shopeame-api-v2/products');
  }

}