import { ProductserviceService } from './../productservice.service';

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-productpage',
  templateUrl: './productpage.component.html',
  styleUrls: ['./productpage.component.scss']
})
export class ProductpageComponent implements OnInit {
  term;
  product;

  constructor(private productserviceComponent: ProductserviceService){}

  // tslint:disable-next-line: typedef
  ngOnInit(): void{
    this.productserviceComponent.getProducts().subscribe ((res: any) => {
    this.product = res;
    });
  }

}
