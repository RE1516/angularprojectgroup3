import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { ProductpageComponent } from './productpage/productpage.component';
import { ProductmainComponent } from './productmain/productmain.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { MainComponent } from './main/main.component';
import { ParrafoComponent } from './parrafo/parrafo.component';
import { TituloComponent } from './titulo/titulo.component';
import { HeaderComponent } from './header/header.component';
import {MatMenuModule} from '@angular/material/menu';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [AppComponent, HeaderComponent],
  imports: [BrowserModule, AppRoutingModule],

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule ,
    MatMenuModule ,
    HeaderComponent ,

  ],

  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
