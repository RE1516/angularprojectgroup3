import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-parrafo',
  templateUrl: './parrafo.component.html',
  styleUrls: ['./parrafo.component.scss']
})
export class ParrafoComponent implements OnInit {
  @Input() parrafo;

  constructor() { }

  ngOnInit(): void {
  }

}
